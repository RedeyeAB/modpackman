#!/usr/bin/env python3
import hashlib
import requests
import os
import shutil
import sys

# Initalize from config
INSTALL_DIR = ""
with open("pack-location.txt", "r") as f:
    INSTALL_DIR = f.read().strip()


# Apply updates to the actual mod pack
def install():
    print("Updating pack...")
    # (fname, checksum, url)
    mods = read_file("downloads.txt")
    names = [mod[0] for mod in mods]
    for mod in mods:
        if mod[0] in os.listdir(INSTALL_DIR) and hashlib.sha1(open(os.path.join(INSTALL_DIR, mod[0]), 'rb').read()).hexdigest() == mod[1]:
            print("Skipping " + mod[0] + ", already up to date")
        else:
            print(f'Installing {mod[0]} from {mod[2]}...')
            download_obj = requests.get(mod[2], stream=True)
            with open(os.path.join(INSTALL_DIR, mod[0]), "wb") as write_file:
                shutil.copyfileobj(download_obj.raw, write_file)
            print("Done!")

    print("\nRemoving old versions...")
    for jar in os.listdir(INSTALL_DIR):
        if jar not in names and os.path.splitext(jar)[1] == ".jar":
            os.remove(os.path.join(INSTALL_DIR, jar))
            print(f"Removing '{jar}'")

    print("\nFinished updating pack!")


# Using the latest urls, update downloads.txt to match the urls and have the correct sha1
def apply_updates():
    print("Reading update file...")
    mods = read_file("latest-urls.txt")
    print("Downloading new versions of all mods...")
    with open('downloads.txt', 'w') as f:
        f.write('# Format: <jarname> <hex digested sha1> <direct download url>\n')
        for mod in mods:
            print(f"Downloading {mod[0]}...")
            resp = requests.get(mod[1])
            hsh = hashlib.sha1(resp.content)
            f.write(f'{mod[0]} {hsh.hexdigest()} {resp.url}\n')
    print("\nDone downloading updates!")

# Find if any updates are available
def check_updates():
    print("Reading update files...")
    latest = read_file("latest-urls.txt")
    old = read_file("downloads.txt")
    old_urls = [mod[2] for mod in old]

    print("Checking updates...\nThe following mods have updates available:\n")
    for mod in latest:
        resp = requests.get(mod[1])
        if not resp.url in old_urls:
            print(f" -> Found update for {mod[0]}: {resp.url.split('/')[-1]}")
    print("\nFinished checking for updates!")

def read_file(fil):
    strings = set()
    with open(fil) as f:
        for line in f:
            string = line.strip().split()
            if len(line) >= 1 and line[0] != '#':
                # run strip on each element
                string = tuple(map(lambda x: x.strip(), string))
                strings.add(string)
    return strings


if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} <apply_updates|find_updates>")
    sys.exit(-1)
elif sys.argv[1] == 'install':
    install()
elif sys.argv[1] == 'apply_updates':
    apply_updates()
elif sys.argv[1] == 'check_updates':
    check_updates()
else:
    print(f"Usage: {sys.argv[0]} <install|apply_updates|check_updates>")
    sys.exit(-1)
